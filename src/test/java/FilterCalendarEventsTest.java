import com.codeborne.selenide.*;
import org.junit.jupiter.api.DisplayName;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Objects;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class FilterCalendarEventsTest {

    Date date = new Date();
    private int months = 13;

    @Test
    @DisplayName("Интеграционный тест экономического календаря. Получаем значения последних 12 месяцев и пишем в файл")
    public void getLast12MonthsEconomicCalendar(){
        Configuration.browser = "chrome";
        String userAgentValue = "Googlebot/2.1 (+http://www.googlebot.com/bot.html)";
        System.setProperty("chromeoptions.args", "--user-agent=" + userAgentValue);

        open("https://www.mql5.com/en/economic-calendar");
        $(byAttribute("for", "filterDate4")).click();
        $("span").should(exist);
        $(byAttribute("for", "filterImportance1")).click();
        $(byAttribute("for", "filterImportance1")).shouldNotBe(checked);
        $(byAttribute("for", "filterImportance2")).click();
        $(byAttribute("for", "filterImportance2")).shouldNotBe(checked);
        $(byAttribute("for", "filterImportance8")).click();
        $(byAttribute("for", "filterImportance8")).shouldNotBe(checked);
        String importance = $(byAttribute("for", "filterImportance4")).getText();
        ElementsCollection allCurrencies = $$(byName("currencies"));
        for (SelenideElement allCurrency : allCurrencies) {
            if (!Objects.equals(allCurrency.getAttribute("value"), "64")) {
                allCurrency.scrollTo().parent().click();
            }
        }
        $(byAttribute("onclick", "window.fpush('Tradays+MQL5.com+Event+Pageview');")).click();
        Assert.assertTrue($(byText("CHF, Swiss frank")).parent().parent().getText().contains("Country"));
        Assert.assertTrue($(byText("CHF, Swiss frank")).parent().getText().contains("Switzerland"));
        Assert.assertTrue($(byXpath("//*[@id=\"eventContentPanel\"]/div[1]/table/tbody/tr[1]/td[2]")).getText().equalsIgnoreCase(importance));
        $(byAttribute("data-content", "history")).click();
        ElementsCollection calendar = $$(by("class", "event-table-history__item"));
        String data = String.format("\n%-20s%-10s%-15s%-15s\n", "| Date", "| Actual", "| Forecast", "| Previous |");

        try (FileWriter fileWriter = createFile()) {
            fileWriter.write(date.toString());
            fileWriter.write(data);
            for (int i = 0; i < months; i++) {
                String date = String.format("%-20s", "| " + calendar.get(i).scrollTo().$(by("class", "event-table-history__date")).getText());
                String actual = "";
                if (calendar.get(i).scrollTo().$(by("class", "event-table-history__actual red")).exists()) {
                    actual = String.format("%-10s", "| " + calendar.get(i).scrollTo().$(by("class", "event-table-history__actual red")).getText() + "&");
                } else {
                    actual = String.format("%-10s", "| " + calendar.get(i).scrollTo().$(by("class", "event-table-history__actual green")).getText() + "&");
                }
                String forecast = String.format("%-15s", "| " + calendar.get(i).scrollTo().$(by("class", "event-table-history__forecast")).getText() + "%");
                String previous = String.format("%-15s\n", "| " + calendar.get(i).scrollTo().$(by("class", "event-table-history__previous")).getText() + "%    |");
                String resultData = date + actual + forecast + previous;
                fileWriter.write(resultData);
            }
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    private FileWriter createFile() throws IOException {
        File file = new File("calendarLogs.txt");
        file.createNewFile();
        return new FileWriter(file, true);
    }
}